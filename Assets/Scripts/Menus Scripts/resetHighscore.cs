﻿using UnityEngine;
using System.Collections;

public class resetHighscore : MonoBehaviour {

	// Use this for initialization
	public void OnClick () {
		PlayerPrefs.SetInt ("highscore", 0);
		PlayerPrefs.SetInt ("highscore L1", 0);
		PlayerPrefs.SetInt ("highscore L2", 0);
		PlayerPrefs.SetInt ("highscore L3", 0);
		PlayerPrefs.SetInt ("highscore L4", 0);
		PlayerPrefs.SetInt ("highscore L5", 0);
		PlayerPrefs.SetInt ("highscore L6", 0);
		PlayerPrefs.SetInt ("highscore L7", 0);
	}
}
