﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class textGetHSL1 : MonoBehaviour {

	Text highscoreText;
	private int highScore;
	// Use this for initialization
	void Start () {
		highscoreText = GetComponent<Text>();
	}
	
	// Update is called once per frame
	void Update () {
		if (GameControllerL1.finish) 
		{
			highscoreText.text = "Highscore: " + highScore;
			highScore = PlayerPrefs.GetInt ("highscore L1");
		}
	}
}
