﻿using UnityEngine;
using System.Collections;

public class PauseControllerL1 : MonoBehaviour {

	public bool paused = false;
	public GameObject pauseCanvasP;
	public GameObject pauseCamera;
	public GameObject[] inGameP = new GameObject[7];
	// Use this for initialization
	void Start () 
	{
		inGameP [0] = GameObject.Find("Ground Platform");
		inGameP [1] = GameObject.Find("Left Wall");
		inGameP [2] = GameObject.Find("Right Wall");
		inGameP [3] = GameObject.Find("camera1");
		inGameP [4] = GameObject.Find("player");
		inGameP [5] = GameObject.Find("Left Wall (1)");
		inGameP [6] = GameObject.Find("Ground Platform (2)");
	}
	
	// Update is called once per frame
	void Update () 
	{
		if (Input.GetKeyDown (KeyCode.Escape) && paused == false) 
		{	
			paused = true;
			pauseCanvasP.SetActive(true);
			pauseCamera.SetActive(true);
			foreach (GameObject _obj in inGameP) 
			{
				
				_obj.SetActive (false);
			}
			Time.timeScale = 0.0f;
		} 
		else if (paused == true && Input.GetKeyDown (KeyCode.Escape)) 
		{
			paused = false;
			Time.timeScale = 1.0f;
			pauseCanvasP.SetActive(false);
			pauseCamera.SetActive(false);
			foreach (GameObject _obj in inGameP) {
				
				_obj.SetActive (true);
			}
		}
	}
}
