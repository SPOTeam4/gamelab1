﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class deathCondtionTextL1 : MonoBehaviour {

	public Text deathText;
	// Use this for initialization
	void Awake () {
		deathText = GetComponent<Text> ();
	}
	
	// Update is called once per frame
	void Update () {
		if (ScoreL1.myTime == 0) 
		{
			deathText.text = "Ran out of time.";
		}
		if (GameControllerL1.fellOut) 
		{
			deathText.text = "Fell out of the map.";
		}
		if (GameControllerL1.playerHP == 0) 
		{
			deathText.text = "No HP left.";
		}
	}
}
