﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class textGetHSL6 : MonoBehaviour {

	Text highscoreText;
	private int highScore;
	// Use this for initialization
	void Start () {
		highScore = PlayerPrefs.GetInt("highscore L6");
	}
	
	// Update is called once per frame
	void Update () {
		highscoreText = GetComponent<Text>();
		highscoreText.text = "Highscore: " + highScore;
	}
}
