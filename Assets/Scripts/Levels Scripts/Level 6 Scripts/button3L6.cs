﻿using UnityEngine;
using System.Collections;

public class button3L6 : MonoBehaviour {

	
	public Transform playerCheck;
	public float playerCheckRadius;
	public LayerMask whatIsPlayer;
	private bool playerPress;
	
	public Transform recordingCheck;
	public float recordingCheckRadius;
	public LayerMask whatIsRecording;
	private bool recordingPress;

	public static bool button3;
	// Use this for initialization
	void Start()
	{
		button3 = false;
	}
	void FixedUpdate()
	{
		playerPress = Physics2D.OverlapCircle (playerCheck.position, playerCheckRadius, whatIsPlayer); 
		recordingPress = Physics2D.OverlapCircle (recordingCheck.position, recordingCheckRadius, whatIsRecording); 
	}
	// Update is called once per frame
	void Update ()
	{
		if (playerPress || recordingPress) 
		{
			button3 = true;
		}
		if (!playerPress && !recordingPress) 
		{
			button3 = false;
		}
	}
}