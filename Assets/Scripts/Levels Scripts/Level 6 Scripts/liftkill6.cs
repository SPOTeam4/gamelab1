﻿using UnityEngine;
using System.Collections;

public class liftkill6 : MonoBehaviour {

    private bool Lifthit;
    private bool rec1Lifthit;
    private bool rec2Lifthit;
    private bool rec3Lifthit;

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "player")
        {
            Lifthit = true;
        }
        if (other.tag == "recording 1")
        {
            rec1Lifthit = true;
        }
        if (other.tag == "recording 2")
        {
            rec2Lifthit = true;
        }
        if (other.tag == "recording 3")
        {
            rec3Lifthit = true;
        }
    }
    void OnTriggerExit2D(Collider2D other)
    {
        if (other.tag == "player")
        {
            Lifthit = false;
        }
        if (other.tag == "recording 1")
        {
            rec1Lifthit = false;
        }
        if (other.tag == "recording 2")
        {
            rec2Lifthit = false;
        }
        if (other.tag == "recording 3")
        {
            rec3Lifthit = false;
        }
    }
    void Update()
    {
        if (Lifthit && underLift.under)
        { GameControllerL6.playerHP = 0; }

        if (rec1Lifthit && underLift.rec1Under)
        {
            Laser.recording1Died = true;
        }
        if (rec2Lifthit && underLift.rec2Under)
        {
            Laser.recording2Died = true;
        }
        if (rec3Lifthit && underLift.rec3Under)
        {
            Laser.recording1Died = true;
        }
    }
}
