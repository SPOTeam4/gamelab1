﻿using UnityEngine;
using System.Collections;

public class laser1DisableL6 : MonoBehaviour {
	
	public GameObject laser1;
	
	public Transform playerCheck;
	public float playerCheckRadius;
	public LayerMask whatIsPlayer;
	private bool playerPress;
	
	public Transform recordingCheck;
	public float recordingCheckRadius;
	public LayerMask whatIsRecording;
	private bool recordingPress;

	public static bool button1;
	// Use this for initialization
	void Start()
	{
		laser1 = GameObject.Find("Laser");
		button1 = false;
	}
	void FixedUpdate()
	{
		playerPress = Physics2D.OverlapCircle (playerCheck.position, playerCheckRadius, whatIsPlayer); 
		recordingPress = Physics2D.OverlapCircle (recordingCheck.position, recordingCheckRadius, whatIsRecording); 
	}
	// Update is called once per frame
	void Update ()
	{
		if (playerPress || recordingPress) 
		{
			laser1.SetActive(false);
			button1 = true;
		}
		if (!playerPress && !recordingPress) 
		{
			laser1.SetActive(true);
			button1 = false;
		}
	}
}