﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class deathCondtionTextL6 : MonoBehaviour {

	public Text deathText;
	// Use this for initialization
	void Awake () {
		deathText = GetComponent<Text> ();
	}
	
	// Update is called once per frame
	void Update () {
		if (ScoreL6.myTime == 0) 
		{
			deathText.text = "Ran out of time.";
		}
		if (GameControllerL6.fellOut) 
		{
			deathText.text = "Fell out of the map.";
		}
		if (GameControllerL6.playerHP == 0) 
		{
			deathText.text = "No HP left.";
		}
	}
}
