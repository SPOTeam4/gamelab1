﻿using UnityEngine;
using System.Collections;

public class laser2DisableL6 : MonoBehaviour {
	
	public GameObject laser2;
	
	public Transform playerCheck;
	public float playerCheckRadius;
	public LayerMask whatIsPlayer;
	private bool playerPress;
	
	public Transform recordingCheck;
	public float recordingCheckRadius;
	public LayerMask whatIsRecording;
	private bool recordingPress;

	public static bool button2;
	// Use this for initialization
	void Start()
	{
		laser2 = GameObject.Find("Laser 2");
		button2 = false;
	}
	void FixedUpdate()
	{
		playerPress = Physics2D.OverlapCircle (playerCheck.position, playerCheckRadius, whatIsPlayer); 
		recordingPress = Physics2D.OverlapCircle (recordingCheck.position, recordingCheckRadius, whatIsRecording); 
	}
	// Update is called once per frame
	void Update ()
	{
		if (playerPress || recordingPress) 
		{
			laser2.SetActive(false);
			button2 = true;
		}
		if (!playerPress && !recordingPress) 
		{
			laser2.SetActive(true);
			button2 = false;
		}
	}
}