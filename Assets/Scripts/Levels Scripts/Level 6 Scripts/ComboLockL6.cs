﻿using UnityEngine;
using System.Collections;

public class ComboLockL6 : MonoBehaviour {

	public SpriteRenderer light1;
	public SpriteRenderer light2;
	public SpriteRenderer light3;

	private bool light1On = false;
	private bool light2On;
	private bool light3On;
	// Use this for initialization
	void Start () 
	{

	}
	
	// Update is called once per frame
	void Update () 
	{
		if (button3L6.button3) 
		{
			if (!laser2DisableL6.button2 && !laser1DisableL6.button1) 
			{
				light1.color = Color.green;
				light1On = true;
			}
		} 
		else if (!button3L6.button3) 
		{
			light1.color = Color.red;
			light1On = false;
			light2.color = Color.red;
			light2On = false;
			light3.color = Color.red;
			light3On = false;
			exitDoorDown();
		}

		if (laser1DisableL6.button1 && light1On) 
		{
			if (!laser2DisableL6.button2) 
			{
				light2.color = Color.green;
				light2On = true;
			}
		} 
		else if (!laser1DisableL6.button1) 
		{
			light2.color = Color.red;
			light2On = false;
			light3.color = Color.red;
			light3On = false;
			exitDoorDown();
		}

		if (laser2DisableL6.button2 && light1On && light2On) 
		{
			light3.color = Color.green;
			light3On = true;
			exitDoorUp();
		} 
		else if (!laser2DisableL6.button2) 
		{
			light3.color = Color.red;
			light3On = false;
			exitDoorDown();
		}
	}
	void exitDoorUp()
	{
		float speedUp = 7f;
		Vector2 newPostionUp = transform.position;

		if (newPostionUp.y <= 359.52) 
		{
			transform.Translate (Vector2.up * speedUp * Time.deltaTime);
		}
	}
	void exitDoorDown()
	{
		float speedDown = 9f;
		Vector2 newPostionDown = transform.position;
		
		if (newPostionDown.y > 347.35f) 
		{
			transform.Translate (Vector2.down * speedDown * Time.deltaTime);
		}
	}
}
