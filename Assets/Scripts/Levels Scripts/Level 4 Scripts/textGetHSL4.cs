﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class textGetHSL4 : MonoBehaviour {

	Text highscoreText;
	private int highScore;
	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void FixedUpdate () {
		highScore = PlayerPrefs.GetInt("highscore L4");
		highscoreText = GetComponent<Text>();
		highscoreText.text = "Highscore: " + highScore;
	}
}
