﻿using UnityEngine;
using System.Collections;

public class Laser : MonoBehaviour {

	public static bool recording1Died;
	public static bool recording2Died;
	public static bool recording3Died;

	void Start()
	{
		recording1Died = false;
		recording2Died = false;
		recording3Died = false;
	}

    void OnTriggerEnter2D(Collider2D other)
    {
		if (other.tag == "player") {
			GameControllerLT.playerHP = GameControllerLT.playerHP - 100;
		}
		if (other.tag == "recording 1") 
		{
			recording1Died = true;
		}
		if (other.tag == "recording 2") 
		{
			recording2Died = true;
		}
		if (other.tag == "recording 3") 
		{
			recording3Died = true;
		}
    }
}
