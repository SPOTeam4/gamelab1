﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class RollerballUserController1 : MonoBehaviour 
{

	public float maxSpeed;
	public float speed;
	public float jumpPower;

	private bool grounded;
	
	public static int playerHP = 100;

	public GameObject victoryCanvas;
	public GameObject camera2;

	private Rigidbody2D rb2d;

	public BoxCollider2D document;
	public GameObject documentPaper;
	public CircleCollider2D playerColl;
	public bool onDocument;

	public TextMesh recText;

	public BoxCollider2D zoneJ;
	public bool inZoneJ;
	private bool noJump = false;

	public bool jumped;
	
	// Use this for initialization
	void Start () 
	{
		rb2d = GetComponent<Rigidbody2D> ();
		playerHP = 100;
		jumped = false;
	}
	void OnCollisionEnter2D (Collision2D other)
	{
		if (other.gameObject.layer == 8 || other.gameObject.layer == 11 || other.gameObject.layer == 15 << 17) 
		{
			grounded = true;
		}
	}
	void OnCollisionExit2D (Collision2D other)
	{
		if (other.gameObject.layer == 8 || other.gameObject.layer == 11 || other.gameObject.layer == 15 << 17) 
		{
			grounded = false;
		}
	}
	void FixedUpdate ()
	{
		inZoneJ = GetComponent<CircleCollider2D>().IsTouching(zoneJ);
		if (inZoneJ) {
			noJump = true;
		} else
			noJump = false;

		Vector3 easeVelocityX = rb2d.velocity;
		easeVelocityX.y *= 1f;
		easeVelocityX.z = 0.0f;
		easeVelocityX.x *= 0.9f;
		Vector3 easeVelocityY = rb2d.velocity;
		easeVelocityY.y *= 1f;
		easeVelocityY.z = 0.0f;
		easeVelocityY.x *= 1f;

		if (grounded) {
			rb2d.velocity = easeVelocityX;
		} else rb2d.velocity = easeVelocityY;
		float Horizontal = Input.GetAxis ("Horizontal");
		rb2d.AddForce ((Vector2.right * speed) * Horizontal);
		if (rb2d.velocity.x > maxSpeed) 
		{
			rb2d.velocity = new Vector2(maxSpeed,rb2d.velocity.y);
		}
		
		if (rb2d.velocity.x < -maxSpeed) 
		{
			rb2d.velocity = new Vector2(-maxSpeed,rb2d.velocity.y);
		}
	}
	// Update is called once per frame
	void Update () 
	{
		if (Input.GetKeyDown (KeyCode.Space) && grounded && !noJump) 
		{
			rb2d.AddForce(Vector2.up * jumpPower);
			jumped = true;
		}
		if (jumped == true && grounded) 
		{
			jumped = false;
		}
		if (Input.GetAxis ("Horizontal") < 0.1f) 
		{
			transform.localScale = new Vector3(3, 3, 3);
			recText.transform.localScale = new Vector3(0.5f, 0.5f, 0.5f);
		}
		
		if (Input.GetAxis ("Horizontal") > 0.1f) 
		{
			transform.localScale = new Vector3(3, 3, 3);
		}

		onDocument = GetComponent<CircleCollider2D>().IsTouching(document);
		if (onDocument) 
		{
			GameControllerLT.collected++;
			Score.Points = Score.Points + 1000;
			Destroy(documentPaper);
		}
	}
}
