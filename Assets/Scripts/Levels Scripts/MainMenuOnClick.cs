﻿using UnityEngine;
using System.Collections;

public class MainMenuOnClick : MonoBehaviour {

	// Use this for initialization
	public void OnClick () {
		Time.timeScale = 1.0f;
		Application.LoadLevel("Main Menu");
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
