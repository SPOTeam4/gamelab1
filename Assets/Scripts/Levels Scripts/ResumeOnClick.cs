using UnityEngine;
using System.Collections;

public class ResumeOnClick : MonoBehaviour 
{
	public static bool pauseClick = false;
	// Use this for initialization
	void Awake()
	{
		pauseClick = false;
	}
	public void OnMouseDown () 
	{
		pauseClick = true;
	}
	
	// Update is called once per frame

}
