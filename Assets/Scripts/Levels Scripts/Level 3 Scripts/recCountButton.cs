﻿using UnityEngine;
using System.Collections;

public class recCountButton : MonoBehaviour {
	
	public GameObject laser1;
	public GameObject laser2;
	
	public Transform playerCheck;
	public float playerCheckRadius;
	public LayerMask whatIsPlayer;
	private bool playerPress;
	
	public Transform recording1Check;
	public float recording1CheckRadius;
	public LayerMask whatIsRecording1;
	private bool recording1Press;

	public Transform recording2Check;
	public float recording2CheckRadius;
	public LayerMask whatIsRecording2;
	private bool recording2Press;

	public Transform recording3Check;
	public float recording3CheckRadius;
	public LayerMask whatIsRecording3;
	private bool recording3Press;

	// Use this for initialization
	void Start () {
	}

	void FixedUpdate ()
	{
		playerPress = Physics2D.OverlapCircle (playerCheck.position, playerCheckRadius, whatIsPlayer); 
		recording1Press = Physics2D.OverlapCircle (recording1Check.position, recording1CheckRadius, whatIsRecording1);
		recording2Press = Physics2D.OverlapCircle (recording2Check.position, recording2CheckRadius, whatIsRecording2); 
		recording3Press = Physics2D.OverlapCircle (recording3Check.position, recording3CheckRadius, whatIsRecording3); 
	}

	// Update is called once per frame
	void Update () {

		if (playerPress || recording1Press || recording2Press || recording3Press)
		{
			laser1.SetActive(false);
			laser2.SetActive(true);
		}
		else if (playerPress == false && recording1Press == false && recording2Press == false && recording3Press == false)
		{
			laser1.SetActive(true);
			laser2.SetActive(true);
		}
		if ((playerPress == true && recording1Press == true) || (playerPress == true && recording2Press == true) || (playerPress == true && recording3Press == true) || (recording1Press == true && recording2Press == true) || (recording2Press == true && recording3Press == true))
		{
			laser1.SetActive(false);
			laser2.SetActive(false);
		}
	

	}
}
