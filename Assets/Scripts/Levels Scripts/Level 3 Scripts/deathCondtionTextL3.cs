﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class deathCondtionTextL3 : MonoBehaviour {

	public Text deathText;
	// Use this for initialization
	void Awake () {
		deathText = GetComponent<Text> ();
	}
	
	// Update is called once per frame
	void Update () {
		if (ScoreL3.myTime == 0) 
		{
			deathText.text = "Ran out of time.";
		}
		if (GameControllerL3.fellOut) 
		{
			deathText.text = "Fell out of the map.";
		}
		if (GameControllerL3.playerHP == 0) 
		{
			deathText.text = "No HP left.";
		}
	}
}
