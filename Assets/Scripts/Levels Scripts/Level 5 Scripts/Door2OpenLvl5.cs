﻿using UnityEngine;
using System.Collections;

public class Door2OpenLvl5 : MonoBehaviour {

	public GameObject Door2;
	public Transform playerCheck;
	public float playerCheckRadius;
	public LayerMask whatIsPlayer;
	private bool playerPress;
	
	public Transform recordingCheck;
	public float recordingCheckRadius;
	public LayerMask whatIsRecording;
	private bool recordingPress;
	// Use this for initialization
	void Start()
	{
		Door2 = GameObject.FindGameObjectWithTag ("Door 2");
	}
	void FixedUpdate()
	{
		playerPress = Physics2D.OverlapCircle (playerCheck.position, playerCheckRadius, whatIsPlayer); 
		recordingPress = Physics2D.OverlapCircle (recordingCheck.position, recordingCheckRadius, whatIsRecording);
	}
	
	// Update is called once per frame
	void Update () {
		if (playerPress || recordingPress) 
		{
			Door2Up ();
		} else if (!playerPress && !recordingPress) 
		{
			Door2Down ();
		}
		
	}
	private void Door2Up()
	{
		float speedDoor2Up = 8.25f;
		if (Door2.transform.position.y < 190.91f) 
		{
			Door2.transform.Translate (Vector2.up * speedDoor2Up * Time.deltaTime);
		}
	}
	private void Door2Down()
	{
		float speedDoor2Down = 14.75f;
		if (Door2.transform.position.y > 184.55f) 
		{
			Door2.transform.Translate (Vector2.down * speedDoor2Down * Time.deltaTime);
		}
	}
}
