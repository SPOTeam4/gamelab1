﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class ScoreL5 : MonoBehaviour
{

	public Text scoreText;
	public static int Points = 15000;
	public int highscore;
	public string minutes;
	public string seconds;
	public Text timeText;
	public static int myTime = 180;

	private bool routineStarted;


	void OnGUI() {
//		GUILayout.Label("score: " + Points);
//		GUILayout.Label("Highscore: " + highscore);
//		GUILayout.Label ("Collectables: " + GameControllerL5.collected + "/1");
//		GUI.Box (new Rect (0,Screen.height - 50,100,50), "Score: " + Points);
		GUI.Box (new Rect (Screen.width - 125,Screen.height - 25,125,25), "Highscore: " + highscore);

//		GUI.Box (new Rect (0,Screen.height - 50,100,50), "Collectables: " + GameControllerL5.collected + "/1");
		int minutes = Mathf.FloorToInt(myTime / 60F);
		int seconds = Mathf.FloorToInt(myTime - minutes * 60);
		string niceTime = string.Format("{0:0}:{01:00}", minutes, seconds);
		GUI.Box (new Rect (Screen.width - 100,30,100,25), "Time Left: " + niceTime);
		GUI.Box (new Rect (Screen.width - 100,0,100,25), "Score: " + Points);
	}

	void Start() 
	{   
		Points = 15000;
		myTime = 180;
		highscore = PlayerPrefs.GetInt("highscore L5");
		StartCoroutine ("countPoints");
		StartCoroutine ("countTime");
	}

	void Update()
	{
		if (Points == 0) {
			StopCoroutine ("countPoints");
		}
		if (myTime == 0 || (GameControllerL5.finish || GameControllerL5.gameOver)) 
		{
			StopCoroutine ("countTime");
		}
		if (endGoalL5.ended || GameControllerL5.gameOver) 
		{
			StopCoroutine("countPoints");
			if (Points > highscore && (endGoalL5.ended && !GameControllerL5.gameOver))
			{
				highscore = Points;
				PlayerPrefs.SetInt ("highscore L5", highscore);

			}
		}
			

		}

//		Points -= Time.deltaTime;
//		text.text = Points.ToString();
//		text.text = Points.ToString ("0");
	

	IEnumerator countPoints()
	{
		while (Points > 0) {
			yield return new WaitForSeconds (1);
			Points = Points - 100;
		}
	}
	IEnumerator countTime()
	{
		while (myTime > 0) {
			yield return new WaitForSeconds (1);
			myTime = myTime - 1;
		}
	}
}

		
	




	
	
	
	
	
	
	
	
	
	
	
	