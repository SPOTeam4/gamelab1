﻿using UnityEngine;
using System.Collections;

public class Door1OpenLvl5 : MonoBehaviour {

	public GameObject Door1;
	public Transform playerCheck;
	public float playerCheckRadius;
	public LayerMask whatIsPlayer;
	private bool playerPress;
	
	public Transform recordingCheck;
	public float recordingCheckRadius;
	public LayerMask whatIsRecording;
	private bool recordingPress;
	// Use this for initialization
	void Start()
	{
		Door1 = GameObject.FindGameObjectWithTag ("Door 1 New");
	}
	void FixedUpdate()
	{
		playerPress = Physics2D.OverlapCircle (playerCheck.position, playerCheckRadius, whatIsPlayer); 
		recordingPress = Physics2D.OverlapCircle (recordingCheck.position, recordingCheckRadius, whatIsRecording);
	}

	// Update is called once per frame
	void Update () {
		if (playerPress || recordingPress) 
		{
			Door1Up ();
		} else if (!playerPress && !recordingPress) 
		{
			Door1Down ();
		}
		
	}
	private void Door1Up()
	{
		float speedDoor1Up = 8.25f;
		if (Door1.transform.position.y < 190.91f) 
		{
			Door1.transform.Translate (Vector2.up * speedDoor1Up * Time.deltaTime);
		}
	}
	private void Door1Down()
	{
		float speedDoor1Down = 14.75f;
		if (Door1.transform.position.y > 184.55f) 
		{
			Door1.transform.Translate (Vector2.down * speedDoor1Down * Time.deltaTime);
		}
	}
}
