﻿using UnityEngine;
using System.Collections;

public class Button1Laser1Dis : MonoBehaviour {


	public GameObject laser1;

	public Transform playerCheck;
	public float playerCheckRadius;
	public LayerMask whatIsPlayer;
	private bool playerPress;
	
	public Transform recordingCheck;
	public float recordingCheckRadius;
	public LayerMask whatIsRecording;
	private bool recordingPress;
	
	void FixedUpdate ()
	{
		playerPress = Physics2D.OverlapCircle (playerCheck.position, playerCheckRadius, whatIsPlayer); 
		recordingPress = Physics2D.OverlapCircle (recordingCheck.position, recordingCheckRadius, whatIsRecording); 
	}
	
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {

		if (playerPress || recordingPress) {
			laser1.SetActive (false);
			
		} else if (!playerPress && !recordingPress) 
		{
			laser1.SetActive (true);
			
		}
		
		
	}
}

