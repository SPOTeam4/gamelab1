﻿using UnityEngine;
using System.Collections;

public class LiftLvl5 : MonoBehaviour {

	private GameObject Lift1;

	public Transform playerCheck;
	public float playerCheckRadius;
	public LayerMask whatIsPlayer;
	private bool playerPress;
	
	public Transform recordingCheck;
	public float recordingCheckRadius;
	public LayerMask whatIsRecording;
	private bool recordingPress;
	// Use this for initialization
	void Start ()
	{
		Lift1 = GameObject.FindGameObjectWithTag ("Lift 1");
		
	}
	void FixedUpdate ()
	{
		playerPress = Physics2D.OverlapCircle (playerCheck.position, playerCheckRadius, whatIsPlayer); 
		recordingPress = Physics2D.OverlapCircle (recordingCheck.position, recordingCheckRadius, whatIsRecording); 
	}
	// Update is called once per frame
	void Update()
	{
		if (playerPress || recordingPress) {
			Lift1MoveUp ();
		} else if (!playerPress && !recordingPress) {
			Lift1MoveDown();
		}
	}

	private void Lift1MoveUp() 
	{
		//the speed, in units per second, we want to move towards the target
		float speedUp1 = 3.69f;
		
		
		Vector2 newPositionUp1 = Lift1.transform.localPosition;
		//move towards the center of the world (or where ever you like)
		
		if (newPositionUp1.y <= 180.277f)
		{
			Lift1.transform.Translate (Vector2.up * speedUp1 * Time.deltaTime);
		}
	}
	private void Lift1MoveDown ()
	{
		float speedDown1 = 10f;
		Vector2 newPositionDown1 = Lift1.transform.localPosition;
		if (newPositionDown1.y > 152.9f)
		{
			Lift1.transform.Translate (Vector2.down * speedDown1 * Time.deltaTime);
		}
		
	}
}
