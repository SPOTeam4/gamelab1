﻿using UnityEngine;
using System.Collections;

public class PauseController : MonoBehaviour {

	private bool paused = false;
	public GameObject pauseCanvasP;
	public GameObject pauseCamera;
	public GameObject[] inGameP = new GameObject[7];
	// Use this for initialization
	void Start () 
	{
		paused = false;
	}
	
	// Update is called once per frame
	void Update () 
	{
		if (Input.GetKeyDown (KeyCode.Escape) && paused == false) 
		{	
			paused = true;
			pauseCanvasP.SetActive(true);
			pauseCamera.SetActive(true);
			foreach (GameObject _obj in inGameP) 
			{
				
				_obj.SetActive (false);
			}
			Time.timeScale = 0.0f;
		} 
		else if (paused == true && (Input.GetKeyDown (KeyCode.Escape) || ResumeOnClick.pauseClick)) 
		{
			paused = false;
			Time.timeScale = 1.0f;
			pauseCanvasP.SetActive(false);
			pauseCamera.SetActive(false);
			foreach (GameObject _obj in inGameP) {
				
				_obj.SetActive (true);
			}
		}
	}
}
