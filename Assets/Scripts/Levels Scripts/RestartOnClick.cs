﻿using UnityEngine;
using System.Collections;

public class RestartOnClick : MonoBehaviour {

	public int level;
	// Use this for initialization
	public void OnClick () {
		level = Application.loadedLevel;
		Application.LoadLevel (level);
		Time.timeScale = 1.0f;
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
