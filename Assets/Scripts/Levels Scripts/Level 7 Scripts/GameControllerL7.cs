﻿using UnityEngine;
using System.Collections;

public class GameControllerL7 : MonoBehaviour {

    public GameObject[] inGame = new GameObject[15];

    public GameObject gameOverCanvas;
    public GameObject victoryCanvas;

    public static int playerHP = 100;

    public GameObject camera1;
    public GameObject camera2;
    public GameObject camera3;

    private GameObject player;
    private GameObject checkpointFlag;

    public static bool finish;
    public static bool gameOver = false;

    public static int collected = 0;
    public static bool fellOut;
    // Use this for initialization

    void OnGUI()
    {
        if (GUI.Button(new Rect(0, 0, 100, 25), "Recording 1"))
        {
            Recorder.RecInt = 0;
        }
        if (GUI.Button(new Rect(100, 0, 100, 25), "Recording 2"))
        {
            Recorder.RecInt = 1;
        }
        if (GUI.Button(new Rect(200, 0, 100, 25), "Recording 3"))
        {
            Recorder.RecInt = 2;
        }
    }
    void Start()
    {
        player = GameObject.Find("player");
        checkpointFlag = GameObject.Find("Checkpoint");
        playerHP = 100;
        collected = 0;
        gameOver = false;
        fellOut = false;
        finish = false;
        camera1.SetActive(true);
        if (checkpointSave.checkpointSaved)
        {
            player.transform.position = checkpointFlag.transform.position;
        }
        camera2.SetActive(false);
    }


    // Update is called once per frame
    void Update()
    {

        if (player.transform.position.y <= 15f || playerHP <= 0 || ScoreL7.myTime <= 0)
        {
            if (player.transform.position.y <= 15f)
            {
                fellOut = true;
            }
            gameOver = true;
            gameOverCanvas.SetActive(true);
            camera2.SetActive(true);
            foreach (GameObject _obj in inGame)
            {

                _obj.SetActive(false);
            }
            camera1.SetActive(false);
        }
        if (endGoalL7.ended == true)
        {
            victoryCanvas.SetActive(true);
            camera2.SetActive(true);
            finish = true;
            foreach (GameObject _obj in inGame)
            {

 				_obj.SetActive(false);
            }
            camera1.SetActive(false);
        }
    }
}
