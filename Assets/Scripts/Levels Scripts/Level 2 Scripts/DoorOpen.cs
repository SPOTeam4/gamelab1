﻿using UnityEngine;
using System.Collections;

public class DoorOpen : MonoBehaviour {

	public GameObject Door2;
	
	public Transform playerCheck;
	public float playerCheckRadius;
	public LayerMask whatIsPlayer;
	private bool playerPress;
	
	public Transform recordingCheck;
	public float recordingCheckRadius;
	public LayerMask whatIsRecording;
	private bool recordingPress;

	public GameObject laser3;
	// Use this for initialization
	void Start()
	{
		Door2 = GameObject.FindGameObjectWithTag ("Door 2");
	}
	void FixedUpdate()
	{
		playerPress = Physics2D.OverlapCircle (playerCheck.position, playerCheckRadius, whatIsPlayer); 
		recordingPress = Physics2D.OverlapCircle (recordingCheck.position, recordingCheckRadius, whatIsRecording); 
	}

	// Update is called once per frame
	void Update () {
		if (playerPress || recordingPress) 
		{
			Door2Down();
		} else if (!playerPress && !recordingPress) 
		{
			Door2Up();
		}
		if (Laser2Lvl4.button1On && (playerPress || recordingPress))
		{
			laser3.SetActive(false);
		} else if (!Laser2Lvl4.button1On || (!playerPress && !recordingPress)) 
		{
			laser3.SetActive(true);
		}
		
	}
	private void Door2Up()
	{
		float speedDoor2Up = 8.25f;
		if (Door2.transform.position.y < 40.94f) 
		{
			Door2.transform.Translate (Vector2.up * speedDoor2Up * Time.deltaTime);
		}
	}
	private void Door2Down()
	{
		float speedDoor2Down = 14.75f;
		if (Door2.transform.position.y > 32.41f) 
		{
			Door2.transform.Translate (Vector2.down * speedDoor2Down * Time.deltaTime);
		}
	}
}
