﻿using UnityEngine;
using System.Collections;

public class GameControllerL2 : MonoBehaviour {

	public GameObject[] inGame = new GameObject[15];

	public GameObject gameOverCanvas;
	public GameObject victoryCanvas;
	
	public static int playerHP = 100; 

	public GameObject camera1;
	public GameObject camera2;
	public GameObject camera3;

	private GameObject player;
	private GameObject checkpointFlag;

	public static bool finish;
	public static bool gameOver = false;

	public static int collected = 0;
	public static bool fellOut;
	// Use this for initialization

	void OnGUI()
	{
		if (GUI.Button(new Rect (0,0,100,25), "Recording 1"))
		{
			Recorder.RecInt = 0;
		}
	}
	void Start () {
		player = GameObject.Find("player");
		checkpointFlag = GameObject.Find ("Checkpoint");
		playerHP = 100;
		collected = 0;
		gameOver = false;
		finish = false;
		fellOut = false;
		if (checkpointSave.checkpointSaved) 
		{
			player.transform.position = checkpointFlag.transform.position;
		}
		foreach(GameObject _obj in inGame)
		{
			
			_obj.SetActive(true);
		}

	}

	
	// Update is called once per frame
	void Update () {
		GameObject recording1 = GameObject.Find ("Recording 1");
		GameObject recording2 = GameObject.Find ("Recording 2");
		GameObject recording3 = GameObject.Find ("Recording 3");
		if (player.transform.position.y <= 15f || playerHP <= 0 || ScoreL2.myTime <= 0) 
		{	
			if (player.transform.position.y <= 15f)
			{
				fellOut = true;
			}
			gameOver = true;
			gameOverCanvas.SetActive(true);
			camera2.SetActive(true);
			if (recording1 != null)
			{
				recording1.transform.position = new Vector2 (999, 999);
				Destroy (recording2);
			}
			if (recording2 != null)
			{
				recording2.transform.position = new Vector2 (999, 999);
				Destroy (recording2);
			}
			if (recording3 != null)
			{
				recording3.transform.position = new Vector2 (999, 999);
				Destroy (recording3);
			}
			foreach(GameObject _obj in inGame)
			{
				
				_obj.SetActive(false);
			}
			camera1.SetActive(false);
		}
		if (endGoalL2.ended == true) {
			victoryCanvas.SetActive (true);
			camera2.SetActive (true);
			finish = true;
			if (recording1 != null)
			{
				Destroy (recording1);
			}
			if (recording1 != null)
			{
				Destroy (recording1);
			}
			if (recording3 != null)
			{
				Destroy (recording3);
			}
			foreach(GameObject _obj in inGame)
			{
				
				_obj.SetActive(false);
			}
			camera1.SetActive(false);
		}
	}
}
