﻿using UnityEngine;
using System.Collections;

public class Button1Press : MonoBehaviour 
{
	public Transform playerCheck;
	public float playerCheckRadius;
	public LayerMask whatIsPlayer;
	private bool playerPress;

	public Transform recordingCheck;
	public float recordingCheckRadius;
	public LayerMask whatIsRecording;
	private bool recordingPress;

	private GameObject Lift1;
	
	// Use this for initialization
	void Start () 
	{
		Lift1 = GameObject.FindGameObjectWithTag ("Lift 1");
	}
	void FixedUpdate ()
	{
		playerPress = Physics2D.OverlapCircle (playerCheck.position, playerCheckRadius, whatIsPlayer); 
		recordingPress = Physics2D.OverlapCircle (recordingCheck.position, recordingCheckRadius, whatIsRecording); 
	}
	// Update is called once per frame
	void Update () 
	{
		if (playerPress == true || recordingPress == true) 
		{
			Vector2 newPositionUp1 = Lift1.transform.localPosition;
			if (newPositionUp1.x <= 2.46f)
			{
				Lift1Up();
			}
		} 
		else if (playerPress == false && recordingPress == false) 
		{
			Vector2 newPositionDown1 = Lift1.transform.localPosition;
			if (newPositionDown1.x > 0.02f)
			{
				Lift1Down ();
			}
		}
	}
	private void Lift1Up() 
	{
		//the speed, in units per second, we want to move towards the target
		float speedUp1 = 5.5f;
		//move towards the center of the world (or where ever you like)
		
		
		Lift1.transform.Translate (Vector2.up * speedUp1 * Time.deltaTime);
		
		
	}
	private void Lift1Down()
	{
		float speedDown1 = 10f;
		
		Lift1.transform.Translate (Vector2.down * speedDown1 * Time.deltaTime);
	}
}
