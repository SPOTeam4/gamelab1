﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class textGetHSL2 : MonoBehaviour {

	Text highscoreText;
	private int highScore;
	// Use this for initialization
	void Start () {
		highScore = PlayerPrefs.GetInt("highscore L2");
		highscoreText = GetComponent<Text>();
		highscoreText.text = "Highscore: " + highScore;
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
