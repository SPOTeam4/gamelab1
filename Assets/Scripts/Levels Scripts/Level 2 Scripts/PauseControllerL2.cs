﻿using UnityEngine;
using System.Collections;

public class PauseControllerL2 : MonoBehaviour {

	private bool paused = false;
	public GameObject pauseCanvasP;
	public GameObject pauseCamera;
	public GameObject resumeButton;
	public GameObject camera1;
	public GameObject[] inGameP = new GameObject[15];
	// Use this for initialization
	void Start () 
	{

	}
	
	// Update is called once per frame
	void Update () 
	{
		if (Input.GetKeyDown (KeyCode.Escape) && paused == false && (GameControllerL2.finish == false && GameControllerL2.gameOver == false)) 
		{	
			paused = true;
			pauseCanvasP.SetActive(true);
			pauseCamera.SetActive(true);
			foreach (GameObject _obj in inGameP) 
			{
				
				_obj.SetActive (false);
			}
			camera1.SetActive(false);
			Time.timeScale = 0.0f;
		} 
		else if (paused == true && (Input.GetKeyDown (KeyCode.Escape) || ResumeOnClick.pauseClick)) 
		{
			paused = false;
			ResumeOnClick.pauseClick = false;
			Time.timeScale = 1.0f;
			pauseCanvasP.SetActive(false);
			pauseCamera.SetActive(false);
			camera1.SetActive(true);
			foreach (GameObject _obj in inGameP) {
				
				_obj.SetActive (true);
			}
		}
	}
}
