﻿using UnityEngine;
using System.Collections;

public class Laser2Lvl4 : MonoBehaviour {

	public GameObject laser2;

	public Transform playerCheck;
	public float playerCheckRadius;
	public LayerMask whatIsPlayer;
	private bool playerPress;

	public Transform recordingCheck;
	public float recordingCheckRadius;
	public LayerMask whatIsRecording;
	private bool recordingPress;

	public static bool button1On;

	void FixedUpdate()
	{
		playerPress = Physics2D.OverlapCircle (playerCheck.position, playerCheckRadius, whatIsPlayer); 
		recordingPress = Physics2D.OverlapCircle (recordingCheck.position, recordingCheckRadius, whatIsRecording); 
	}

	// Use this for initialization
	void Start () {
		button1On = false;
	}

	// Update is called once per frame
	void Update () {
		if (playerPress || recordingPress) {
			laser2.SetActive (false);
			button1On = true;

		} else if (!playerPress && !recordingPress) 
		{
			laser2.SetActive (true);
			button1On = false;

		}
		
	}
	}

