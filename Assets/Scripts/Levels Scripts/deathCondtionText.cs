﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class deathCondtionText : MonoBehaviour {

	public Text deathText;
	// Use this for initialization
	void Awake () {
		deathText = GetComponent<Text> ();
	}
	
	// Update is called once per frame
	void Update () {
		if (Score.myTime == 0) 
		{
			deathText.text = "Ran out of time.";
		}
		if (GameControllerLT.fellOut) 
		{
			deathText.text = "Fell out of the map.";
		}
		if (GameControllerLT.playerHP == 0) 
		{
			deathText.text = "No HP left.";
		}
	}
}
