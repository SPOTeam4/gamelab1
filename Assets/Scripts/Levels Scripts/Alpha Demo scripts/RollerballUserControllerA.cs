﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class RollerballUserControllerA : MonoBehaviour 
{

	public float maxSpeed;
	public float speed;
	public float jumpPower;


	public Transform groundCheck;
	public float groundCheckRadius;
	public LayerMask whatIsGround;
	private bool grounded;
	

	private bool playerRec;
	public Transform playerRecCheck;
	public float playerRecCheckRadius;
	public LayerMask whatIsPlayerRec;
	
	public static int playerHP = 100;

	public GameObject victoryCanvas;
	public GameObject camera2;

	private Rigidbody2D rb2d;

	public BoxCollider2D document;
	public GameObject documentPaper;
	public CircleCollider2D playerColl;
	public bool onDocument;

	public TextMesh recText;

	public BoxCollider2D zoneJ;
	public bool inZoneJ;
	private bool noJump = false;
	// Use this for initialization
	void Start () 
	{
		rb2d = GetComponent<Rigidbody2D> ();
		playerHP = 100;
	}
	void FixedUpdate ()
	{
		inZoneJ = GetComponent<CircleCollider2D>().IsTouching(zoneJ);
		if (inZoneJ) {
			noJump = true;
		} else
			noJump = false;
		grounded = Physics2D.OverlapCircle (groundCheck.position, groundCheckRadius, whatIsGround);
		playerRec = Physics2D.OverlapCircle (playerRecCheck.position, playerRecCheckRadius, whatIsPlayerRec);
		Vector3 easeVelocityX = rb2d.velocity;
		easeVelocityX.y *= 1f;
		easeVelocityX.z = 0.0f;
		easeVelocityX.x *= 0.9f;
		Vector3 easeVelocityY = rb2d.velocity;
		easeVelocityY.y *= 1f;
		easeVelocityY.z = 0.0f;
		easeVelocityY.x *= 1f;

		if (grounded) {
			rb2d.velocity = easeVelocityX;
		} else rb2d.velocity = easeVelocityY;
		float Horizontal = Input.GetAxis ("Horizontal");
		rb2d.AddForce ((Vector2.right * speed) * Horizontal);
		if (rb2d.velocity.x > maxSpeed) 
		{
			rb2d.velocity = new Vector2(maxSpeed,rb2d.velocity.y);
		}
		
		if (rb2d.velocity.x < -maxSpeed) 
		{
			rb2d.velocity = new Vector2(-maxSpeed,rb2d.velocity.y);
		}
	}
	// Update is called once per frame
	void Update () 
	{
		if (Input.GetKeyDown (KeyCode.Space) && (grounded || playerRec) && !noJump) 
		{
			rb2d.AddForce(Vector2.up * jumpPower);
		}
		if (Input.GetAxis ("Horizontal") < 0.1f) 
		{
			transform.localScale = new Vector3(4.5f, 4.5f, 4.5f);
			recText.transform.localScale = new Vector3(0.5f, 0.5f, 0.5f);
		}
		
		if (Input.GetAxis ("Horizontal") > 0.1f) 
		{
			transform.localScale = new Vector3(4.5f, 4.5f, 4.5f);
		}

		onDocument = GetComponent<CircleCollider2D>().IsTouching(document);
		if (onDocument) 
		{
			GameControllerLT.collected++;
			Score.Points = Score.Points + 1000;
			Destroy(documentPaper);
		}
	}
}
