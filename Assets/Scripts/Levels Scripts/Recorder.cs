﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using System.Linq;


public class Recorder : MonoBehaviour 
{
	public bool recording;
	public bool playing;

	public GameObject recPrefab;
	public GameObject recTextOn;

	public BoxCollider2D zone;
	public bool inZone;
	private bool noRecord = false;

	public GameObject recording1;
	public GameObject recording2;
	public GameObject recording3;

	private GameObject recording1Shadow;
	private GameObject recording2Shadow;
	private GameObject recording3Shadow;

	public static int RecInt = 0;
	public int recordingLimit = 3;

	public bool rec0;
	public bool rec1;
	public bool rec2;

	public bool play0;
	public bool play1;
	public bool play2;

	public List<Vector3> pos0 = new List<Vector3>();
	public List<Vector3> pos1 = new List<Vector3>();
	public List<Vector3> pos2 = new List<Vector3>();

	public List<bool> jump0 = new List<bool> ();
	public List<bool> jump1 = new List<bool> ();
	public List<bool> jump2 = new List<bool> ();

	public float groundDiff1;
	public float groundDiff2;
	public float groundDiff3;

	public float jumpDiff1;
	public float jumpDiff2;
	public float jumpDiff3;

	private bool falling1;
	private bool falling2;
	private bool falling3;
	
	SpriteRenderer r1Sprite;
	SpriteRenderer r2Sprite;
	SpriteRenderer r3Sprite;

	private bool alreadySpawnedR1;
	private bool alreadySpawnedR2;
	private bool alreadySpawnedR3;

	private bool timeBridgeRec;
	// Use this for initialization
	void Start () 
	{
		RecInt = 0;
		recordingLimit = 3;
	}

	void OnTriggerEnter2D(Collider2D other)
	{
		if (other.tag == "recZone") 
		{
			timeBridgeRec = true;
		}
	}
	void OnTriggerExit2D(Collider2D other)
	{
		if (other.tag == "recZone") 
		{
			timeBridgeRec = false;
		}
	}
	// Update is called once per frame
	void Update () 
	{
		if (Application.loadedLevel == 7) {
			if (Laser.recording1Died) {
				StopCoroutine ("playPos0");
				play0 = false;
				if (recording1 != null)
				{
					recording1.transform.position = new Vector2 (999, 999);
					Destroy (recording1);
					Debug.Log ("Recording 1 died");
				}
			}
			if (Laser.recording2Died) {
				StopCoroutine ("playPos1");
				play1 = false;
				if (recording2 != null)
				{
					recording2.transform.position = new Vector2 (999, 999);
					Destroy (recording2);
					Debug.Log ("Recording 2 died");
				}

			}
			if (Laser.recording3Died) {
				StopCoroutine ("playPos2");
				play2 = false;
				if (recording3 != null)
				{
					recording3.transform.position = new Vector2 (999, 999);
					Destroy (recording3);
					Debug.Log ("Recording 3 died");
				}
;
			}
		}
		if (Application.loadedLevel == 6) {
			if (LaserL3.recording1Died) {
				StopCoroutine ("playPos0");
				play0 = false;
				if (recording1 != null)
				{
				recording1.transform.position = new Vector2 (999, 999);
				Destroy (recording1);
				Debug.Log ("Recording 1 died");
				}
	
			}
			if (LaserL3.recording2Died) {
				StopCoroutine ("playPos1");
				play1 = false;
				if (recording2 != null)
				{
					recording2.transform.position = new Vector2 (999, 999);
					Destroy (recording2);
					Debug.Log ("Recording 2 died");
				}

			}
			if (LaserL3.recording3Died) {
				StopCoroutine ("playPos2");
				play2 = false;
				if (recording3 != null)
				{
					recording3.transform.position = new Vector2 (999, 999);
					Destroy (recording3);
					Debug.Log ("Recording 3 died");
				}

			}
		}

		inZone = GetComponent<CircleCollider2D>().IsTouching(zone);
		if (inZone) {
			noRecord = true;
		} else
			noRecord = false;

		if (Input.GetKeyDown(KeyCode.R) && noRecord == false)
		{
			if (RecInt < recordingLimit)
			{
				if (RecInt == 0)
				{
					if (rec0 == false && rec1 == false && rec2 == false && play0 == false) 
					{
						if (pos0.Count != 0)
						{
							pos0.Clear();
							jump0.Clear();
						}
						rec0 = true;
						recTextOn.SetActive(true);
						Debug.Log ("Recording Started");
						StartCoroutine ("Record0");
					}
					else if (rec0 == true || rec1 == true || rec2 == true)
					{
						if (rec0)
						{
							transform.position = pos0[1];
							StopCoroutine("Record0");
							rec0 = false;
							recTextOn.SetActive(false);	
							Debug.Log ("Recording Stopped");
						}
						if (rec1)
						{
							transform.position = pos1[1];
							StopCoroutine("Record1");
							rec1 = false;
							recTextOn.SetActive(false);	
							Debug.Log ("Recording Stopped");
						}
						if (rec2)
						{
							transform.position = pos2[1];
							StopCoroutine("Record2");
							rec2 = false;
							recTextOn.SetActive(false);	
							Debug.Log ("Recording Stopped");
						}
					}
				}
			
				if (RecInt == 1)
				{
					if (rec1 == false && play1 == false && rec0 == false && rec2 == false)
					{
						if (pos1.Count != 0)
						{
							pos1.Clear();
						}
						rec1 = true;
						StartCoroutine ("Record1");
						recTextOn.SetActive(true);
						Debug.Log ("Recording Started");
					}
					else if (rec0 == true || rec1 == true || rec2 == true)
					{
						if (rec0)
						{
							transform.position = pos0[1];
							StopCoroutine("Record0");
							rec0 = false;
							recTextOn.SetActive(false);	
							Debug.Log ("Recording Stopped");
						}
						if (rec1)
						{
							transform.position = pos1[1];
							StopCoroutine("Record1");
							rec1 = false;
							recTextOn.SetActive(false);	
							Debug.Log ("Recording Stopped");
						}
						if (rec2)
						{
							transform.position = pos2[1];
							StopCoroutine("Record2");
							rec2 = false;
							recTextOn.SetActive(false);	
							Debug.Log ("Recording Stopped");
						}
					}
				}
			

				if (RecInt == 2)
				{
					if (rec2 == false && play2 == false && rec0 == false && rec1 == false)
					{
						if (pos2.Count != 0)
						{
							pos2.Clear();
						}
						rec2 = true;
						StartCoroutine ("Record2");
						recTextOn.SetActive(true);
						Debug.Log ("Recording Started");
					}
					else if (rec0 == true || rec1 == true || rec2 == true)
					{
						if (rec0)
						{
							transform.position = pos0[1];
							StopCoroutine("Record0");
							rec0 = false;
							recTextOn.SetActive(false);	
							Debug.Log ("Recording Stopped");
						}
						if (rec1)
						{
							transform.position = pos1[1];
							StopCoroutine("Record1");
							rec1 = false;
							recTextOn.SetActive(false);	
							Debug.Log ("Recording Stopped");
						}
						if (rec2)
						{
							transform.position = pos2[1];
							StopCoroutine("Record2");
							rec2 = false;
							recTextOn.SetActive(false);	
							Debug.Log ("Recording Stopped");
						}
					}
				}
			}
			else Debug.Log ("Out of Recordings");
		}

		if (Input.GetKeyDown(KeyCode.P))
		{
			if (RecInt == 0)
			{
				if (play0 == false && rec0 == false)
				{
					if (pos0.Count > 0)
					{
					play0 = true;
					recording1 = Instantiate(recPrefab, pos0[1], Quaternion.identity)
						as GameObject;
					recording1.transform.localScale = new Vector3(1.3f,1.3f,1);
					recording1.name = "Recording 1";
					recording1.tag = "recording 1";
					r1Sprite = recording1.GetComponent<SpriteRenderer>();
					if (Application.loadedLevel == 6)
					{
						recording1.layer = 15;
						recording1.transform.localScale = new Vector3(0.98f,0.98f,1);
					}
					if (Application.loadedLevel == 10 || Application.loadedLevel == 3)
					{
						recording1.transform.localScale = new Vector3(4.5f, 4.5f, 4.5f);
					}
						if (Application.loadedLevel == 9)
					{
							recording1.transform.localScale = new Vector3 (2.8f, 2.8f, 2.8f);
					}
						if (Application.loadedLevel == 4)
						{
							recording1.transform.localScale = new Vector3 (2, 2, 1);
						}
					StartCoroutine ("playPos0");
					//StartCoroutine ("playRotation");
					Debug.Log ("Playing Started");
					groundDiff1 = 0;
					jumpDiff1 = 0;
					}
				}
				else if (play0 == true && rec0 == false) 
				{
					StopCoroutine ("playPos0");
					play0 = false;
					recording1.transform.position = new Vector2(999,999);
					Destroy(recording1);
					if (recording1Shadow != null)
					{
						Destroy(recording1Shadow);
					}
					alreadySpawnedR1 = false;
					Debug.Log ("Playing Stopped");
					groundDiff1 = 0;
					jumpDiff1 = 0;
				}
			}

			if (RecInt == 1)
			{
				if (play1 == false && rec1 == false)
				{
					if (pos1.Count > 0)
					{
						play1 = true;
						recording2 = Instantiate(recPrefab, pos1[1], Quaternion.identity)
							as GameObject;
						recording2.transform.localScale = new Vector3(1.3f,1.3f,1);
						recording2.name = "Recording 2";
						recording2.tag = "recording 2";
						r2Sprite = recording2.GetComponent<SpriteRenderer>();
						if (Application.loadedLevel == 6)
						{
							recording2.layer = 16;
							recording2.transform.localScale = new Vector3(0.98f,0.98f,1);
						}
						if (Application.loadedLevel == 10 || Application.loadedLevel == 3)
						{
							recording2.transform.localScale = new Vector3(4.5f, 4.5f, 4.5f);
						}
						if (Application.loadedLevel == 4)
						{
							recording2.transform.localScale = new Vector3 (2, 2, 1);
						}
						if (Application.loadedLevel == 9)
						{
							recording2.transform.localScale = new Vector3 (2.8f, 2.8f, 2.8f);
						}
						StartCoroutine ("playPos1");
						//StartCoroutine ("playRotation");
						Debug.Log ("Playing Started");
						groundDiff2 = 0;
					}
				}
				else if (play1 == true && rec1 == false) 
				{
					StopCoroutine ("playPos1");
					play1 = false;
					recording2.transform.position = new Vector2(999,999);
					Destroy(recording2);
					Debug.Log ("Playing Stopped");
					groundDiff2 = 0;
				}
			}

			if (RecInt == 2)
			{
				if (play2 == false && rec2 == false)
				{
					if (pos2.Count > 0)
					{
						play2 = true;
						recording3 = Instantiate(recPrefab, pos2[1], Quaternion.identity)
							as GameObject;
						recording3.transform.localScale = new Vector3(1.3f,1.3f,1);
						recording3.name = "Recording 3";
						recording3.tag = "recording 3";
						r3Sprite = recording3.GetComponent<SpriteRenderer>();
						if (Application.loadedLevel == 6)
						{
							recording3.layer = 17;
							recording3.transform.localScale = new Vector3(0.98f,0.98f,1);
						}
						if (Application.loadedLevel == 10 || Application.loadedLevel == 3)
						{
							recording3.transform.localScale = new Vector3(4.5f, 4.5f, 4.5f);
						}
						if (Application.loadedLevel == 4)
						{
							recording3.transform.localScale = new Vector3 (2, 2, 1);
						}
						if (Application.loadedLevel == 9)
						{
							recording3.transform.localScale = new Vector3 (2.8f, 2.8f, 2.8f);
						}
					
						StartCoroutine ("playPos2");
						//StartCoroutine ("playRotation");
						Debug.Log ("Playing Started");
						groundDiff3 = 0;
					}
				}
				else if (play2 == true && rec2 == false) 
				{
					StopCoroutine ("playPos2");
					play2 = false;
					recording3.transform.position = new Vector2(999,999);
					Destroy(recording3);
					Debug.Log ("Playing Stopped");
					groundDiff3 = 0;
				}
			}
		}
	}

	IEnumerator Record0()
	{
		Debug.Log ("Recording");
		while (rec0 == true) 
		{
			yield return new WaitForFixedUpdate ();
			pos0.Add(transform.position);
			jump0.Add(RollerballUserController7.jumped);
		}
	}
	IEnumerator Record1()
	{
		Debug.Log ("Recording");
		while (rec1 == true) 
		{
			Debug.Log ("hi");
			yield return new WaitForFixedUpdate ();
			pos1.Add(transform.position);
			jump1.Add(RollerballUserController7.jumped);
		}
	}
	IEnumerator Record2()
	{
		Debug.Log ("Recording");
		while (rec2 == true) 
		{
			yield return new WaitForFixedUpdate ();
			pos2.Add(transform.position);
			jump2.Add(RollerballUserController7.jumped);
		}
	}
	IEnumerator playPos0()
	{
		Debug.Log ("Playing Recording");
		if (pos0.Count > 0) 
		{

			for (int posInt = 1; posInt < pos0.Count; posInt++)
			{

				Ray2D ray1;
				Vector2 modPos0 = recording1.transform.position;
				modPos0.x = recording1.transform.position.x;
				modPos0.y = recording1.transform.position.y - ((recording1.GetComponent<Transform>().localScale.x) * (recording1.GetComponent<CircleCollider2D>().radius));
				ray1 = new Ray2D (modPos0, recording1.transform.up * 999.9f);
				//Debug.DrawRay(ray1.origin, ray1.direction * 999.9f);
				RaycastHit2D hit1 = Physics2D.Raycast(ray1.origin, ray1.direction, 999.9f, 11 << 18);
				//Debug.Log (hit1.collider.tag);

				if (timeBridgeRec)
				{
					if (hit1 != null && hit1.collider != null && hit1.collider.tag == "time grid rec")
					{
							groundDiff1 = hit1.distance;
							Debug.Log("ray1 hit.");
							Color tempSprite = r1Sprite.color;
							tempSprite.a = 0;
							r1Sprite.color = tempSprite;
							CircleCollider2D r1Coll = recording1.GetComponent<CircleCollider2D>();
							r1Coll.enabled = false;

							if (alreadySpawnedR1 == false)
							{
								recording1Shadow = Instantiate(recPrefab, pos0[posInt], Quaternion.identity)
									as GameObject;
								recording1Shadow.transform.localScale = new Vector3(1.3f,1.3f,1);
								recording1Shadow.name = "Recording 1 Shadow";
								recording1Shadow.tag = "recording 1";
								alreadySpawnedR1 = true;
								if (Application.loadedLevel == 6)
								{
									recording1Shadow.layer = 15;
									recording1Shadow.transform.localScale = new Vector3(0.98f,0.98f,1);
								}
								if (Application.loadedLevel == 10 || Application.loadedLevel == 3)
								{
									recording1Shadow.transform.localScale = new Vector3(4.5f, 4.5f, 4.5f);
								}
								if (Application.loadedLevel == 9)
								{
									recording1Shadow.transform.localScale = new Vector3 (4, 4, 4);
								}
								if (Application.loadedLevel == 4)
								{
									recording1Shadow.transform.localScale = new Vector3 (2, 2, 1);
								}
							}
							if (recording1Shadow != null)
							{
							Vector3 tempPosR1S = recording1Shadow.transform.position;
							tempPosR1S.x = recording1.transform.position.x;
							tempPosR1S.y = recording1.transform.position.y + groundDiff1 + jumpDiff1;
							recording1Shadow.transform.position = tempPosR1S;
							}
						}
						else if (hit1 != null && hit1.collider != null && hit1.collider.tag != "time grid rec")
						{
							Color tempSprite = r1Sprite.color;
							tempSprite.a = 1;
							r1Sprite.color = tempSprite;
							CircleCollider2D r1Coll = recording1.GetComponent<CircleCollider2D>();
							r1Coll.enabled = true;
							if (recording1Shadow != null)
							{
							Destroy(recording1Shadow);
							}
							alreadySpawnedR1 = false;
						}

					Ray2D ray1G;
					ray1G = new Ray2D (modPos0, -recording1.transform.up * 99.9f);
					Debug.DrawRay(ray1G.origin, ray1G.direction * 99.9f);
					RaycastHit2D hit1G = Physics2D.Raycast(ray1G.origin, ray1G.direction, 99.9f);
					if (hit1G != null && hit1G.collider != null && hit1G.collider.tag == "ground")
					{
						if (hit1G.distance > 6f)
						{
							falling1 = true;
						}
						if (hit1G.distance > 0.01f && falling1)
						{
							jump0[posInt] = false;
						}
						else if (hit1G.distance < 0.01f && falling1)
						{
							falling1 = false;
						}
						if (jump0[posInt] == true && falling1 == false)
						{
							jumpDiff1 = hit1G.distance;
						}
						else if (jump0[posInt] == false)
						{
							jumpDiff1 = 0;
						}
					}
					else if(hit1G != null && hit1G.collider != null && hit1G.collider.tag != "ground")
					{
						jumpDiff1 = 0;
					}
				}

				yield return new WaitForFixedUpdate ();
				recording1.transform.position = pos0[posInt]; 
			}
		}
		recording1.transform.position = new Vector2(999,999);
		Destroy(recording1);
		if (recording1Shadow != null)
		{
			Destroy(recording1Shadow);
		}
		alreadySpawnedR1 = false;
		play0 = false;
		groundDiff1 = 0;
		jumpDiff1 = 0;
	}
	IEnumerator playPos1()
	{
		Debug.Log ("Playing Recording");
		if (pos1.Count > 0) 
		{
			
			for (int posInt = 1; posInt < pos1.Count; posInt++)
			{
				Ray2D ray2;
				
				Vector2 modPos1 = recording2.transform.position;
				modPos1.x = recording2.transform.position.x;
				modPos1.y = recording2.transform.position.y - ((recording2.GetComponent<Transform>().localScale.x) * (recording2.GetComponent<CircleCollider2D>().radius));
				ray2 = new Ray2D (modPos1, recording2.transform.up * 999.9f);
				Debug.DrawRay(ray2.origin, ray2.direction * 999.9f);
				RaycastHit2D hit2 = Physics2D.Raycast(ray2.origin, ray2.direction, 999.9f, 11 << 18);
				if(hit2 != null && hit2.collider != null && hit2.collider.tag == "time grid rec")
				{
					groundDiff2 = hit2.distance;
					Debug.Log("ray2 hit.");
					Color tempSprite = r2Sprite.color;
					tempSprite.a = 0;
					r2Sprite.color = tempSprite;
					CircleCollider2D r2Coll = recording2.GetComponent<CircleCollider2D>();
					r2Coll.enabled = false;
					
					if (alreadySpawnedR2 == false)
					{
						recording2Shadow = Instantiate(recPrefab, pos0[posInt], Quaternion.identity)
							as GameObject;
						recording2Shadow.transform.localScale = new Vector3(1.3f,1.3f,1);
						recording2Shadow.name = "Recording 2 Shadow";
						recording2Shadow.tag = "recording 2";
						alreadySpawnedR2 = true;
						if (Application.loadedLevel == 6)
						{
							recording2Shadow.layer = 15;
							recording2Shadow.transform.localScale = new Vector3(0.98f,0.98f,1);
						}
						if (Application.loadedLevel == 10 || Application.loadedLevel == 3)
						{
							recording2Shadow.transform.localScale = new Vector3(4.5f, 4.5f, 4.5f);
						}
						if (Application.loadedLevel == 9)
						{
							recording2Shadow.transform.localScale = new Vector3 (4, 4, 4);
						}
						if (Application.loadedLevel == 4)
						{
							recording2Shadow.transform.localScale = new Vector3 (2, 2, 1);
						}
					}
					Vector3 tempPosR2S = recording2Shadow.transform.position;
					tempPosR2S.x = recording2.transform.position.x;
					tempPosR2S.y = recording2.transform.position.y + groundDiff2 + jumpDiff2;
					recording2Shadow.transform.position = tempPosR2S;
				}
				else if (hit2 != null && hit2.collider != null && hit2.collider.tag != "time grid rec")
				{
					Color tempSprite = r2Sprite.color;
					tempSprite.a = 1;
					r2Sprite.color = tempSprite;
					CircleCollider2D r2Coll = recording2.GetComponent<CircleCollider2D>();
					r2Coll.enabled = true;
					Destroy(recording2Shadow);
					alreadySpawnedR2 = false;
				}

				Ray2D ray2G;
				ray2G = new Ray2D (modPos1, -recording2.transform.up * 99.9f);
				Debug.DrawRay(ray2G.origin, ray2G.direction * 99.9f);
				RaycastHit2D hit2G = Physics2D.Raycast(ray2G.origin, ray2G.direction, 99.9f);
				if (hit2G != null && hit2G.collider != null && hit2G.collider.tag == "ground")
				{
					Debug.Log(hit2G.distance);
					if (hit2G.distance > 6f)
					{
						falling2 = true;
					}
					if (hit2G.distance > 0.01f && falling2)
					{
						jump1[posInt] = false;
					}
					else if (hit2G.distance < 0.01f && falling2)
					{
						falling2 = false;
					}
					if (jump1[posInt] == true && falling2 == false)
					{
						jumpDiff2 = hit2G.distance;
					}
					else if (jump1[posInt] == false)
					{
						jumpDiff2 = 0;
					}
				}
				else if(hit2G != null && hit2G.collider != null && hit2G.collider.tag != "ground")
				{
					jumpDiff2 = 0;
				}

				yield return new WaitForFixedUpdate ();
				recording2.transform.position = pos1[posInt]; 
			}
		}
		recording2.transform.position = new Vector2(999,999);
		Destroy(recording2);
		Destroy(recording2Shadow);
		play1 = false;
		groundDiff2 = 0;
	}
	IEnumerator playPos2()
	{
		Debug.Log ("Playing Recording");
		if (pos2.Count > 0) 
		{
			
			for (int posInt = 1; posInt < pos2.Count; posInt++)
			{
				Ray2D ray3;
				Vector2 modPos2 = recording3.transform.position;
				modPos2.x = recording3.transform.position.x;
				modPos2.y = recording3.transform.position.y - ((recording3.GetComponent<Transform>().localScale.x) * (recording3.GetComponent<CircleCollider2D>().radius));
				ray3 = new Ray2D (modPos2, recording3.transform.up * 999.9f);
				Debug.DrawRay(ray3.origin, ray3.direction * 999.9f);
				RaycastHit2D hit3 = Physics2D.Raycast(ray3.origin, ray3.direction, 999.9f, 11 << 18);
				if(hit3 != null && hit3.collider != null && hit3.collider.tag == "time grid rec")
				{
					groundDiff3 = hit3.distance;
					Debug.Log("ray3 hit.");
					Color tempSprite = r3Sprite.color;
					tempSprite.a = 0;
					r3Sprite.color = tempSprite;
					CircleCollider2D r3Coll = recording3.GetComponent<CircleCollider2D>();
					r3Coll.enabled = false;
					
					if (alreadySpawnedR3 == false)
					{
						recording3Shadow = Instantiate(recPrefab, pos0[posInt], Quaternion.identity)
							as GameObject;
						recording3Shadow.transform.localScale = new Vector3(1.3f,1.3f,1);
						recording3Shadow.name = "Recording 2 Shadow";
						recording3Shadow.tag = "recording 2";
						alreadySpawnedR3 = true;
						if (Application.loadedLevel == 6)
						{
							recording3Shadow.layer = 15;
							recording3Shadow.transform.localScale = new Vector3(0.98f,0.98f,1);
						}
						if (Application.loadedLevel == 10 || Application.loadedLevel == 3)
						{
							recording3Shadow.transform.localScale = new Vector3(4.5f, 4.5f, 4.5f);
						}
						if (Application.loadedLevel == 9)
						{
							recording3Shadow.transform.localScale = new Vector3 (4, 4, 4);
						}
						if (Application.loadedLevel == 4)
						{
							recording3Shadow.transform.localScale = new Vector3 (2, 2, 1);
						}
					}
					Vector3 tempPosR3S = recording3Shadow.transform.position;
					tempPosR3S.x = recording3.transform.position.x;
					tempPosR3S.y = recording3.transform.position.y + groundDiff3 + jumpDiff3;
					recording3Shadow.transform.position = tempPosR3S;
				}
				else if (hit3 != null && hit3.collider != null && hit3.collider.tag != "time grid rec")
				{
					Color tempSprite = r3Sprite.color;
					tempSprite.a = 1;
					r3Sprite.color = tempSprite;
					CircleCollider2D r3Coll = recording3.GetComponent<CircleCollider2D>();
					r3Coll.enabled = true;
					Destroy(recording3Shadow);
					alreadySpawnedR3 = false;
				}

				Ray2D ray3G;
				ray3G = new Ray2D (modPos2, -recording3.transform.up * 99.9f);
				Debug.DrawRay(ray3G.origin, ray3G.direction * 99.9f);
				RaycastHit2D hit3G = Physics2D.Raycast(ray3G.origin, ray3G.direction, 99.9f);
				if (hit3G != null && hit3G.collider != null && hit3G.collider.tag == "ground")
				{
					Debug.Log(hit3G.distance);
					if (hit3G.distance > 6f)
					{
						falling3 = true;
					}
					if (hit3G.distance > 0.01f && falling3)
					{
						jump0[posInt] = false;
					}
					else if (hit3G.distance < 0.01f && falling3)
					{
						falling3 = false;
					}
					if (jump0[posInt] == true && falling3 == false)
					{
						jumpDiff3 = hit3G.distance;
					}
					else if (jump0[posInt] == false)
					{
						jumpDiff3 = 0;
					}
				}
				else if(hit3G != null && hit3G.collider != null && hit3G.collider.tag != "ground")
				{
					jumpDiff3 = 0;
				}

				yield return new WaitForFixedUpdate ();
				recording3.transform.position = pos2[posInt]; 
			}
		}
		recording3.transform.position = new Vector2(999,999);
		Destroy(recording3);
		Destroy(recording3Shadow);
		play2 = false;
		groundDiff3 = 0;
	}
}
