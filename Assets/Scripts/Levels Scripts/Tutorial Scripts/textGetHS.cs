﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class textGetHS : MonoBehaviour {

	Text highscoreText;
	private int highScore;
	// Use this for initialization
	void Start () {
		highScore = PlayerPrefs.GetInt("highscore");
	}
	
	// Update is called once per frame
	void Update () {
		highscoreText = GetComponent<Text>();
		highscoreText.text = "Highscore: " + highScore;
	}
}
