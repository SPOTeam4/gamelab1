﻿using UnityEngine;
using System.Collections;

public class underLift : MonoBehaviour {


    public static bool under;
	public static bool rec1Under;
	public static bool rec2Under;
	public static bool rec3Under;

	void Start()
	{
		under = false;
		rec1Under = false;
		rec2Under = false;
		rec3Under = false;
	}
    void OnTriggerEnter2D(Collider2D other)
    {
		if (other.tag == "player")
		{
			under = true;
		}
		if (other.tag == "recording 1")
		{
			rec1Under = true;
		}
		if (other.tag == "recording 2")
		{
			rec2Under = true;
		}
		if (other.tag == "recording 3")
		{
			rec3Under = true;
		}
    }
    void OnTriggerExit2D(Collider2D other)
    {
		if (other.tag == "player")
		{
			under = false;
		}
		if (other.tag == "recording 1")
		{
			rec1Under = false;
		}
		if (other.tag == "recording 2")
		{
			rec2Under = false;
		}
		if (other.tag == "recording 3")
		{
			rec3Under = false;
		}
    }
    // Update is called once per frame
    void Update () {
	
	}
}
