﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class RecText : MonoBehaviour {

	public TextMesh fadeText;
	public float fadeSpeed = 2f;
	private bool inFade = false;
	private bool doFadeOut;
	private bool doFadeIn;

	public GameObject player;
	
	void Awake()
	{
		fadeText.color = Color.red;
	}
	
	void Update()
	{
		if (Application.loadedLevel == 8) {
			transform.position = player.transform.position;
		}
		if (fadeText.color.a >= 0.85f && inFade == false) 
		{
			doFadeOut = true;
		}
		if (doFadeOut)
		{
			inFade = true;
			fadeOut();
			if (fadeText.color.a <= 0.15f)
			{
				inFade = false;
				doFadeOut = false;
			}
		}
		if (fadeText.color.a <= 0.15f && inFade == false)
		{
			doFadeIn = true;
		}
		if (doFadeIn)
		{
			fadeIn();
			inFade = true;
			if (fadeText.color.a >= 0.85f)
			{
				inFade = false;
				doFadeIn = false;
			}
		}
	}
	
	
	void fadeOut()
	{
		// Lerp the colour of the image between itself and transparent.
		fadeText.color = Color.Lerp(fadeText.color, Color.clear, fadeSpeed * Time.deltaTime);
	}
	void fadeIn()
	{
		fadeText.color = Color.Lerp (fadeText.color, Color.red, fadeSpeed * Time.deltaTime);
	}	
}   
