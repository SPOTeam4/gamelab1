﻿using UnityEngine;
using System.Collections;

public class Button1PressLT : MonoBehaviour {
	
	public Transform playerCheck;
	public float playerCheckRadius;
	public LayerMask whatIsPlayer;
	private bool playerPress;

	public Transform recordingCheck;
	public float recordingCheckRadius;
	public LayerMask whatIsRecording;
	private bool recordingPress;
	private GameObject Lift2;

	void FixedUpdate ()
	{
		playerPress = Physics2D.OverlapCircle (playerCheck.position, playerCheckRadius, whatIsPlayer); 
		recordingPress = Physics2D.OverlapCircle (recordingCheck.position, recordingCheckRadius, whatIsRecording); 
	}
	// Use this for initialization
	void Start () {
		Lift2 = GameObject.FindGameObjectWithTag ("Lift 2");

	}
	
	// Update is called once per frame
	void Update ()
	{
		if (playerPress || recordingPress) 
		{
			Lift2MoveUp ();
		}
		if (!playerPress && !recordingPress) 
		{
			Lift2MoveDown ();
		}
	}
	private void Lift2MoveUp()
	{
		float speedLift2Up = 5.75f;
		
		Vector2 newPositionUp2 = Lift2.transform.localPosition;

		if (newPositionUp2.y <= 3.7666f) 
		{
			Lift2.transform.Translate (Vector2.up * speedLift2Up * Time.deltaTime);
		}
	}
	private void Lift2MoveDown()
	{
		float speedLift2Down = 10.25f;
		Vector2 newPositionDown2 = Lift2.transform.localPosition;

		if (newPositionDown2.y > 0.585f) 
		{
			Lift2.transform.Translate (Vector2.down * speedLift2Down * Time.deltaTime);
		}
	}
}
