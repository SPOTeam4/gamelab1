﻿using UnityEngine;
using System.Collections;

public class laser1Disable : MonoBehaviour {

	public GameObject laser1;
	public GameObject Door2;
	private bool cratePress = false;
	// Use this for initialization
	void Start()
	{
		Door2 = GameObject.FindGameObjectWithTag ("Door 2");
	}
	void OnTriggerEnter2D (Collider2D other) 
	{
		if (other.tag == "Crate") 
		{
			cratePress = true;
		}
	}
	void OnTriggerExit2D(Collider2D other)
	{
		if (other.tag == "Crate") 
		{
			cratePress = false;
		}
	}
	// Update is called once per frame
	void Update () {
		if (cratePress) {
			laser1.SetActive (false);
			Door2Up ();
		} else if (!cratePress) 
		{
			laser1.SetActive (true);
			Door2Down ();
		}
	
	}
	private void Door2Up()
	{
		float speedDoor2Up = 8.25f;
		 if (Door2.transform.position.y <= 178.9f) 
		{
			Door2.transform.Translate (Vector2.up * speedDoor2Up * Time.deltaTime);
		}
	}
	private void Door2Down()
	{
		float speedDoor2Down = 14.75f;
		if (Door2.transform.position.y >= 158.58f) 
		{
			Door2.transform.Translate (Vector2.down * speedDoor2Down * Time.deltaTime);
		}
	}
}
