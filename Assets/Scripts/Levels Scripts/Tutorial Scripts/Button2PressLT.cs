﻿using UnityEngine;
using System.Collections;

public class Button2PressLT : MonoBehaviour {

	private GameObject Lift3;
	private GameObject pPlatform1;

	public Transform playerCheck;
	public float playerCheckRadius;
	public LayerMask whatIsPlayer;
	private bool playerPress;

	public Transform recordingCheck;
	public float recordingCheckRadius;
	public LayerMask whatIsRecording;
	private bool recordingPress;
	// Use this for initialization
	void Start () {
		Lift3 = GameObject.FindGameObjectWithTag ("Lift 3");
		pPlatform1 = GameObject.FindGameObjectWithTag("Portable Platform");
	}
	
	// Update is called once per frame
	void FixedUpdate ()
	{
		playerPress = Physics2D.OverlapCircle (playerCheck.position, playerCheckRadius, whatIsPlayer); 
		recordingPress = Physics2D.OverlapCircle (recordingCheck.position, recordingCheckRadius, whatIsRecording); 
	}

	void Update ()
	{
		if (playerPress || recordingPress) 
		{
			Lift3MoveRight ();
			Platform1MoveLeft ();
		}
		if (!playerPress && !recordingPress) 
		{
			Lift3MoveLeft ();
			Platform1MoveRight();
		}
	}
	private void Lift3MoveRight()
	{
		float speedLift3Right = 6f;
		
		Vector2 newPositionRight3 = Lift3.transform.localPosition;

		if (newPositionRight3.x <= 67.4222f) 
		{
			Lift3.transform.Translate (Vector2.right * speedLift3Right * Time.deltaTime);
		}
	}

	private void Lift3MoveLeft()
	{
		float speedLift3Left = 8f;
		Vector2 newPositionLeft3 = Lift3.transform.localPosition;
		
		if (newPositionLeft3.x > 23.2f) 
		{
			Lift3.transform.Translate (Vector2.left * speedLift3Left * Time.deltaTime);
		}
	}

	private void Platform1MoveLeft()
	{
		float speedPlat1Left = 4f;
		
		Vector2 newPositionLeftP1 = pPlatform1.transform.position;
		
		if (newPositionLeftP1.x > 84f) 
		{
			pPlatform1.transform.Translate (Vector2.down * speedPlat1Left * Time.deltaTime);
		}
	}
	private void Platform1MoveRight()
	{
		float speedPlat1Right = 6f;
		
		Vector2 newPositionRightP1 = pPlatform1.transform.position;
		
		if (newPositionRightP1.x < 98f) 
		{
			pPlatform1.transform.Translate (Vector2.up * speedPlat1Right * Time.deltaTime);
		}
	}
}
