﻿using UnityEngine;
using System.Collections;

public class stayOnPlatform : MonoBehaviour {

	public GameObject player;
	public GameObject Lift3;

	private bool attached;
	// Use this for initialization
	void Start () 
	{

	}
	
	// Update is called once per frame
	void Update () 
	{

		if (attached) {
			player.transform.parent = Lift3.transform;
		} else if (!attached) 
		{
			player.transform.parent = null;
		}
	}
	void OnCollisionEnter2D()
	{
		attached = true;
	}
	void OnCollisionExit2D()
	{
		attached = false;
	}
}
