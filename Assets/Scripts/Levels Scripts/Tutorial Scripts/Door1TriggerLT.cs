﻿using UnityEngine;
using System.Collections;

public class Door1TriggerLT : MonoBehaviour {

	private GameObject Door1;
	private bool doorTriggered = false;
	// Use this for initialization
	void Start () {
		Door1 = GameObject.FindGameObjectWithTag ("Door 1");
	}
	
	// Update is called once per frame
	void Update()
	{
		if (doorTriggered) 
		{
			if (Door1.transform.position.y < 41.66f) 
			{
				Door1MoveUp ();
			}
		}
		if (doorTriggered == false) 
		{
			if (Door1.transform.position.y > 35.25f) 
			{
				Door1MoveDown ();
			}
		}
	}

	void OnTriggerEnter2D (Collider2D other)
	{
		doorTriggered = true;
	}
	void OnTriggerExit2D()
	{
		doorTriggered = false;
	}
	private void Door1MoveUp()
	{
		float speedDoorUp = 14.75f;
		

			Door1.transform.Translate (Vector2.up * speedDoorUp * Time.deltaTime);
	}
	private void Door1MoveDown()
	{
		float speedDoorDown = 8.25f;

		Door1.transform.Translate (Vector2.down * speedDoorDown * Time.deltaTime);
	}
}
