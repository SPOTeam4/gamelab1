﻿using UnityEngine;
using System.Collections;

public class Lift1Up : MonoBehaviour {
	

	private GameObject Lift1;
	
	// Use this for initialization
	void Start ()
	{
		Lift1 = GameObject.FindGameObjectWithTag ("Lift 1");
	
	}
	void FixedUpdate ()
	{
		
	}
	// Update is called once per frame
	void OnTriggerStay2D () 
	{
		Lift1MoveUp();
	} 
	void OnTriggerExit2D()
	{
		Lift1MoveDown ();
	}
	private void Lift1MoveUp() 
	{
		//the speed, in units per second, we want to move towards the target
		float speedUp1 = 5.79f;
	

		Vector2 newPositionUp1 = Lift1.transform.localPosition;
		//move towards the center of the world (or where ever you like)
		
		if (newPositionUp1.x <= 1.59f)
		{
			Lift1.transform.Translate (Vector2.up * speedUp1 * Time.deltaTime);
		}
	}
	private void Lift1MoveDown ()
	{
		float speedDown1 = 10f;
		Vector2 newPositionDown1 = Lift1.transform.localPosition;
		if (newPositionDown1.x > 0.08f)
		{
			Lift1.transform.Translate (Vector2.down * speedDown1 * Time.deltaTime);
		}

	}
}
